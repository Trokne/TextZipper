﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextZipper.Core.QuasiReferencing;

namespace TextZipper
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mItNew_Click(object sender, RoutedEventArgs e)
        {
            ClearTextBoxes();
        }

        private void ClearTextBoxes()
        {
            tbSrcText.Clear();
            tbZipText.Clear();
        }

        private void mItOpen_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog()
            {
                CheckFileExists = true,
                DefaultExt = "txt",
                Multiselect = false,
                Filter= "Текстовые файлы (*.txt)|*.txt"
            };
            if (ofd.ShowDialog() == true)
            {
                try
                {
                    tbSrcText.Text = File.ReadAllText(ofd.FileName);
                }
                catch (Exception exeption)
                {
                    MessageBox.Show(exeption.Message, "Ошибка чтения файла", 
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void mItSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void mItExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void udZipPercent_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (udZipPercent.Value is null)
                udZipPercent.Value = 0;
        }

        private void btnZipText_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbSrcText.Text))
            {
                MessageBox.Show("Перед началом сжатия текста убедитесь, что он не пуст и не состоит из одних пробелов.",
                    "Ошибка чтения текста!",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            if (udZipPercent.Value == 100)
                tbZipText.Text = tbSrcText.Text;
            else tbZipText.Text = (new TextAnalizer(tbSrcText.Text, (int)udZipPercent.Value)).GetResult();
        }

        private void tbZipText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
