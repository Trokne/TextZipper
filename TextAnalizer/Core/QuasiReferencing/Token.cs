﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextZipper.Core.QuasiReferencing
{
    public class Token
    {
        public string Content { get; set; }
        public int Frequency { get; set; }
       
        public Token()
        {
            Content = "";
            Frequency = 0;
        }

        public Token(string content, int weight = 0)
        {
            Content = content;
            weight = 0;
        }
    }
}
