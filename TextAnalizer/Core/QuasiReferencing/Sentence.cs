﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextZipper.Core.QuasiReferencing
{
    public class Sentence
    {
        public string EndSymbol { get; set; }
        public StringBuilder Content { get; private set; }
        public List<Token> Tokens { get; set; }
        public int Number { get; set; }
        public int Weight { get; set; }

        public Sentence()
        {
            Content = new StringBuilder();
            Tokens = new List<Token>();
            EndSymbol = ".";
            Number = 0;
            Weight = 0;
        }
    }
}
