﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TextZipper.Core.QuasiReferencing
{
    public class TextAnalizer
    {
        public static HashSet<string> StopWords { get; set; }

        private List<Token> _tokenList;
        private List<Sentence> _sentences;
        private int _zipPercent;
        private string _sourceText;

        public TextAnalizer(string text, int zipPercent)
        {
            _sourceText = text;
            _zipPercent = zipPercent;
            _sentences = new List<Sentence>();
            if (StopWords == null)
                StopWords = new HashSet<string>(
                    File.ReadAllLines(Path.Combine(
                        AppDomain.CurrentDomain.BaseDirectory, 
                        @"Resources\stopwords.txt")));

        }

        public string GetResult()
        {
            GenerateSentences();
            GetTokens();
            CalculateWeights();
            RangeSentences();
            return GetTextFromSentences();
        }

        private string GetTextFromSentences()
        {
            StringBuilder result = new StringBuilder();
            foreach (var sentence in _sentences)
            {
                result.Append(sentence.Content);
                result.Append(sentence.EndSymbol);
            }
            return result.ToString();
        }

        private void RangeSentences()
        {
            _sentences = _sentences.OrderByDescending(s => s.Weight).ToList();
            int totalCount = Convert.ToInt32(Math.Round((double)_sentences.Count * _zipPercent / 100));
            var resultSentences = new List<Sentence>();
            for (int i = 0; i < totalCount; i++)
                resultSentences.Add(_sentences[i]);
            _sentences = resultSentences.OrderBy(s => s.Number).ToList();
        }

        private void CalculateWeights()
        {
            _tokenList.RemoveAll(x => x.Frequency == 1);
            if (_tokenList == null || _tokenList.Count == 0)
                return;
            if (_tokenList[0].Frequency * _tokenList[0].Frequency < int.MaxValue)
                _tokenList.ForEach(x => x.Frequency *= x.Frequency);
            _sentences.ForEach(s => s.Weight = s.Tokens.Sum(t => t.Frequency));
        }

        private void GetTokens()
        {
            HashSet<Token> tokenList = new HashSet<Token>();
            foreach (var sentence in _sentences)
                sentence.Tokens = MyStem.GetLemmas(ref tokenList, sentence.Content.ToString());
            _tokenList = new List<Token>(tokenList.OrderByDescending(x => x.Frequency));
        }

        private void GenerateSentences()
        {
            var sentence = new Sentence();
            int number = 0;
            for (int i = 0; i < _sourceText.Length; i++)
            {
                if (!IsEndSentence(_sourceText[i]))
                    sentence.Content.Append(_sourceText[i]);
                else
                {
                    string endSymbol = _sourceText[i].ToString();
                    while (i + 1 < _sourceText.Length && IsEndSentence(_sourceText[i + 1]))
                    {
                        endSymbol += _sourceText[i + 1];
                        i++;
                    }
                    sentence.EndSymbol = endSymbol;
                    _sentences.Add(sentence);
                    sentence = new Sentence
                    {
                        Number = ++number
                    };
                }
            }
            if (sentence.Content.Length != 0)
                _sentences.Add(sentence);
        }

        private bool IsEndSentence(char symbol)
        {
            return symbol == '.'
                || symbol == '?'
                || symbol == '!';
        }
    }
}
