﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextZipper.Core.QuasiReferencing;

namespace TextZipper.Core
{
    public static class MyStem
    {
        private static string _appPath => AppDomain.CurrentDomain.BaseDirectory;
        private static string _myStem => @"Resources\mystem.exe";
        private static string _args => "-nld";

        public static List<Token> GetLemmas(ref HashSet<Token> tokenList, string sentence)
        {
            string tempPath = Path.Combine(_appPath, @"Temp");
            string inputFile = Path.Combine(tempPath, @"input.txt");
            string outputFile = Path.Combine(tempPath, @"output.txt");
            string myStemExe = Path.Combine(_appPath, _myStem);

            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            File.WriteAllText(inputFile, sentence);
            string args = _args + $" \"{inputFile}\" \"{outputFile}\"";
            StartHiddenProcess(myStemExe, args);
            var lemmas = new List<Token>();
            using (var streamReader = new StreamReader(outputFile))
            {
                string line;
                while((line = streamReader.ReadLine()) != null)
                {
                    if (!TextAnalizer.StopWords.Contains(line))
                    {
                        var token = tokenList.FirstOrDefault(x => x.Content.Equals(line));
                        if (token == null)
                        {
                            token = new Token(line);
                            tokenList.Add(token);
                        }
                        token.Frequency++;
                        lemmas.Add(token);
                    }
                }
            }

            return lemmas;
        }

        private static void StartHiddenProcess(string path, string args)
        {
            var processInfo = new ProcessStartInfo()
            {
                FileName = path,
                Arguments = args,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = false,
                RedirectStandardInput = true,
                RedirectStandardError = true
            };
            var process = Process.Start(processInfo);
            process.WaitForExit();
        }
    }
}
